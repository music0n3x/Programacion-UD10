package com.company;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Enterprise {

    private Integer id;
    private String name;
    private String address;
    private String city;
    private String province;
    private String country;
    private String locale;
    private LocalDateTime createdOn;
    private String nif;
    private int status;

    public Enterprise(String name,String address,String city,String province, String country,String locale,String nif,int status){

        this.id=null;
        this.name=name;
        this.address=address;
        this.city=city;
        this.province=province;
        this.country=country;
        this.locale=locale;
        this.createdOn=LocalDateTime.now();
        this.nif=nif;
        this.status=status;

    }

    public Enterprise(Integer id,String name,String address,String city,String province, String country,String locale,LocalDateTime createdOn,String nif,int status){

        this.id=id;
        this.name=name;
        this.address=address;
        this.city=city;
        this.province=province;
        this.country=country;
        this.locale=locale;
        this.createdOn=createdOn;
        this.nif=nif;
        this.status=status;

    }

    public static int askId(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce la id");
        int id = scanner.nextInt();

        if (validator.validateId(id)){
            return id;
        }else {
            System.out.println("Introduce un id valido");
            askId();
        }
        return 0;
    }

    public static String askName(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el nombre");
        String nombre = scanner.next();

        if (validator.validateName(nombre)){
            return nombre;
        }else {
            System.out.println("Introduce un nombre valido");
            askId();
        }
        return null;
    }

    public static String askAdress(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce la direccion");
        String direccion = scanner.next();

        if (validator.validateAdress(direccion)){
            return direccion;
        }else {
            System.out.println("Introduce una direccion valida");
            askAdress();
        }
        return null;
    }

    public static String askCity(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce la ciudad");
        String ciudad = scanner.next();

        if (validator.validateCity(ciudad)){
            return ciudad;
        }else {
            System.out.println("Introduce un nombre de ciudad valido");
            askCity();
        }
        return null;
    }

    public static String askProvince(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce la provincia");
        String provincia = scanner.next();

        if (validator.validateProvince(provincia)){
            return provincia;
        }else {
            System.out.println("Introduce una provincia valida  ");
            askProvince();
        }
        return null;
    }

    public static String askCountry(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el pais");
        String pais = scanner.next();

        if (validator.validateCountry(pais)){
            return pais;
        }else {
            System.out.println("Introduce un nombre de pais valido");
            askCountry();
        }
        return null;
    }

    public static String askLocale(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el locale");
        String locale = scanner.next();

        if (validator.validateLocale(locale)){
            return locale;
        }else {
            System.out.println("Introduce un locale valido");
            askId();
        }
        return null;
    }

    public static String askNif(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el nif");
        String nif = scanner.next();

        if (validator.validateNif(nif)){
            return nif;
        }else {
            System.out.println("Introduce un nif valido");
            askId();
        }
        return null;
    }

    public static int askStatus(){

        Validator validator = new Validator();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el status");
        int status = scanner.nextInt();

        if (validator.validateStatus(status)){
            return status;
        }else {
            System.out.println("Introduce un status valido");
            askStatus();
        }
        return 0;

    }

    public Integer getId() {
        return id;
    }

    public int getStatus() {
        return 1;
    }
    /*
    public boolean isActive(){
        if (this.status){
            return true;
        }else {
            return false;
        }
    }
    */
    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getLocale() {
        return locale;
    }

    public String getName() {
        return name;
    }

    public String getNif() {
        return nif;
    }

    public String getProvince() {
        return province;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "nombre: "+name+"\n"+
                "address: "+address+"\n"+
                "city: "+city+"\n"+
                "province: "+province+"\n"+
                "country: "+country+"\n"+
                "locale: "+locale+"\n"+
                "nif: "+nif+"\n"+
                "created on: "+createdOn+"\n"+
                "status: "+status+"\n";
    }
}
