package com.company;

public interface EnterpriseRepository {

    boolean save(Enterprise enterprise);
    Enterprise findById(int id);
    Enterprise getById(int id)throws EnterpriseNotFoundException;

}
