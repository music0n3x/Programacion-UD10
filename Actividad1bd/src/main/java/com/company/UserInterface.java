package com.company;

public interface UserInterface {

    String getPassword();
    String getSalt();
    String getUsername();

}
