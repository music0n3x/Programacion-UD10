package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnectionAlt {

    private String ip;
    private String database;
    private String user;
    private String password;
    private Connection connection;


    public MySqlConnectionAlt(String ip, String database, String user, String password){

        this.ip=ip;
        this.database=database;
        this.user=user;
        this.password=password;
        this.loadDriver();

    }

    public MySqlConnectionAlt(String database, String user, String password){

        this.ip="localhost";
        this.database=database;
        this.user=user;
        this.password=password;
        this.loadDriver();

    }

    public Connection getConnection(){

        if (this.connection==null){

            this.connect();

        }

        return connection;
    }

    public void close(){

        //this.connection.close();

    }

    public void connect(){

        try {

            this.connection=DriverManager.getConnection(this.database, this.user, this.password);

        } catch (SQLException ex) {

        // handle any errors
        System.out.println("SQLException: " + ex.getMessage());
        System.out.println("SQLState: " + ex.getSQLState());
        System.out.println("VendorError: " + ex.getErrorCode());

    }

    }

    public void loadDriver(){

        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception ex) {
            System.out.println("Error loading driver: " + ex.getMessage());

        }

    }

}
