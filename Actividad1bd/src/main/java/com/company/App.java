package com.company;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Scanner;

import static com.company.Enterprise.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){

        MySQLConnection mySQLConnection = new MySQLConnection("localhost", "crm_db", "root","micasabonita");

        Connection connection = mySQLConnection.getConnection();

        try {

            if (connection.isValid(1000)){

                System.out.println("Conexion realizada!");

            }

        } catch (SQLException ex){

            System.out.println("Error en la conexion!");

        }


        //printNameAndEnterprise(connection);

        //menu(connection);

        //mySQLConnection.close(connection);

        //Enterprise enterprise=new Enterprise("name","adres","city","province","c","ES","nif",1);
        DbEnterpriseRepository dbEnterpriseRepository = new DbEnterpriseRepository();
        //dbEnterpriseRepository.insert(enterprise);
        //System.out.println(dbEnterpriseRepository.findById(5));
        //dbEnterpriseRepository.findById(5;
        //dbEnterpriseRepository.save(enterprise);
        //printAllEnterprises(connection);
        //dbEnterpriseRepository.login();
        /*
        LocalDate birthday = LocalDate.of(2015, 12, 31);
        User user1 = new User("firstName1","lastName1","email1","phone1","password1",birthday);
        User user2 = new User("firstName2","lastName2","email2","phone2","password2",birthday);
        User[] usersArray=new User[2];
        usersArray[0]=user1;
        usersArray[1]=user2;
        dbEnterpriseRepository.insertUsersArray(usersArray);
        */


        UserRepository userRepository = new UserRepository(1,mySQLConnection);
        PasswordHelper passwordHelper = new PasswordHelper();
        String username = passwordHelper.getUsername();
        String password = passwordHelper.getPassword();
        User user = userRepository.findUserByEmail(username);
        if(user!=null){
            if (passwordHelper.verifyUserCredentials(user,password)){
                System.out.println("El usuario se ha logeado correctamente");
            }else {
                System.out.println("Error en el logueo");
            }
        }

    }

    public static void printNameAndEnterprise(Connection connection){


        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select us.firstName,us.lastName,ent.name from User as us, Enterprise as ent where ent.id=1 OR ent.id=2;\n");
            while (resultSet.next()) {
                System.out.println("nombre: "+resultSet.getString("firstName")+" / apellido: "+resultSet.getString("lastName")+" / empresa: "+resultSet.getString("name"));
            }
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

    }

    public static void insertEnterprise(Connection connection){

        System.out.println("Introduce los datos de la empresa a introducir");
        int id = askId();
        String name = askName();
        String address = askAdress();
        String city = askCity();
        String province = askProvince();
        String country = askCountry();
        String locale = askLocale();
        String nif = askNif();
        int status = 1;
        LocalDateTime nowDateTime = LocalDateTime.now();

        String insertValues = "'"+String.valueOf(id)+"','"+name+"','"+address+"','"+city+"','"+province+"','"+country+"','"+locale+"','"+String.valueOf(nowDateTime)+"','"+nif+"','"+status+"'";

        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate("INSERT INTO Enterprise VALUES ("+insertValues+")");
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }




    }

    public static void printAllEnterprises(Connection connection){

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Enterprise");

            while (resultSet.next()){
                System.out.println("ID: "+resultSet.getString("id"));
                System.out.println("nombre: "+resultSet.getString("name"));
                System.out.println("direccion: "+resultSet.getString("address"));
                System.out.println("ciudad: "+resultSet.getString("city"));
                System.out.println("provicia: "+resultSet.getString("province"));
                System.out.println("pais: "+resultSet.getString("country"));
                System.out.println("locale: "+resultSet.getString("locale"));
                System.out.println("creada en: "+resultSet.getString("createdOn"));
                System.out.println("nif: "+resultSet.getString("nif"));
                System.out.println("estado: "+resultSet.getString("status"));
                System.out.println("\n");

            }

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

    }

    public static void modifew(Connection connection){

        printAllEnterprises(connection);

        System.out.println("Introduce el id de la empresa elegida para modificar datos");

        int id = askId();

        System.out.println("Iniciando modificacion...");
        System.out.println("Introduce los siguientes datos para actualizarlos");

        String name = askName();
        String address = askAdress();
        String city = askCity();
        String province = askProvince();
        String country = askCountry();
        String locale = askLocale();

        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate("UPDATE Enterprise SET name='"+name+"',address='"+address+"',city='"+city+"',province='"+province+"',country='"+country+"',locale='"+locale+"'  WHERE id="+id);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }

    }


    public static void menu(Connection connection){

        Scanner scanner = new Scanner(System.in);

        int eleccion;

        do {

            System.out.println("Introduce una opcion");
            System.out.println("1-Mostrar todas las empresas registradas en la base de datos");
            System.out.println("2-Introducir empresa en la base de datos");
            System.out.println("3-Modificar datos de empresa en la base de datos");
            System.out.println("4-Salir");

            eleccion = scanner.nextInt();

            if (eleccion==1){
                printAllEnterprises(connection);
            }else if (eleccion==2){
                insertEnterprise(connection);
            }else if (eleccion==3){
                modifew(connection);
            }else if (eleccion==4){
                System.out.println("Saliendo");
            }else {
                System.out.println("Introduce una opcion valida");
            }

        }while (eleccion!=4);

    }

    public static void getUserName(){}

}
