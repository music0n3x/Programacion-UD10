package com.company;

import org.apache.commons.codec.digest.DigestUtils;

import java.sql.Connection;
import java.util.Scanner;

public class PasswordHelper implements UserInterface{


    private static String getSha1(String password) {
        return DigestUtils.sha1Hex(password);
    }

    public static String generateSha1(String password){
        return getSha1(password);
    }

    public static boolean verifySha1(String password,String hash){

        return getSha1(password).equalsIgnoreCase(hash);

    }

    public static boolean verifyUserCredentials(UserInterface user,String hash){

        return verifySha1(user.getPassword(),hash);

    }

    public String getPassword(){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce la password");
        String password = scanner.next();
        return getSha1(password);

    }
    public String getSalt(){return "";}

    public String getUsername(){

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el email");
        return scanner.next();

    }

    public boolean login(int pageSize,MySQLConnection connection){

        UserRepository userRepository = new UserRepository(pageSize,connection);

        User user = userRepository.findUserByEmail(getUsername());
        String password = getPassword();
        String passwordOnDatabase = user.getPassword();

        if (password.equalsIgnoreCase(passwordOnDatabase)){
            System.out.println("All right");
            return true;
        }
        System.out.println("error");
        return false;

    }

}
