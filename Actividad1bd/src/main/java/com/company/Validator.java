package com.company;

public class Validator {

    public Validator(){

    }

    public boolean validateId(int num){

        if (validateLength(num,11)){
            return true;
        }else return false;

    }

    public boolean validateName(String word){

        if (validateLength(word,45)){
            return true;
        }else return false;

    }

    public boolean validateAdress(String word){

        if (validateLength(word,45)){
            return true;
        }else return false;

    }

    public boolean validateCity(String word){

        if (validateLength(word,45)){
            return true;
        }else return false;


    }

    public boolean validateProvince(String word){

        if (validateLength(word,45)){
            return true;
        }else return false;

    }

    public boolean validateCountry(String word){

        if (validateLength(word,2)){
            return true;
        }else return false;

    }

    public boolean validateLocale(String word){

        if (validateLength(word,5)){
            return true;
        }else return false;

    }

    public boolean validateNif(String word){

        if (validateLength(word,12)){
            return true;
        }else return false;

    }

    public boolean validateStatus(int status){

        if (status==1 | status==0){
            return true;
        }else {
            return false;
        }

    }


    public boolean validateLength(String word,int length){

        if (word.length()>length){
            return false;
        }else {
            return true;
        }

    }

    public boolean validateLength(int num,int length){

        String numString= String.valueOf(num);

        if (numString.length()>length){
            return false;
        }else {
            return true;
        }

    }


}
