package com.company;

import java.util.ArrayList;

public interface UserReporsitory {

    boolean save(User user);
    User findUserById(int id);
    User getUserById(int id)throws UserNotFoundException;
    User findUserByEmail(String email);
    ArrayList<User> findUserByEnterprise(int id,int page);
    boolean dropUser(int userId);

}
