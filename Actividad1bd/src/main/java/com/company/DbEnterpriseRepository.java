package com.company;


import java.sql.*;
import java.time.LocalDateTime;
import java.util.Scanner;

import static com.company.PasswordHelper.generateSha1;
import static com.company.PasswordHelper.verifySha1;

public class DbEnterpriseRepository implements EnterpriseRepository{

    private MySQLConnection connection;
    private int pageSize;

    DbEnterpriseRepository(){
        this.connection=new MySQLConnection("localhost", "crm_db", "root","micasabonita");
    }

    @Override
    public boolean save(Enterprise enterprise) {

        if (enterprise.getId() != null){
            return this.update(enterprise);
        }

        return this.insertAlex(enterprise);

    }

    @Override
    public Enterprise findById(int id) {

        Connection connection = this.connection.getConnection();
        String SQLQuery = "SELECT * FROM Enterprise WHERE id="+id;

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQLQuery);

            if (resultSet.next()) {
                int id2 = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String address = resultSet.getString("address");
                String city = resultSet.getString("city");
                String province = resultSet.getString("province");
                String country = resultSet.getString("country");
                String locale = resultSet.getString("locale");
                LocalDateTime createdOn = resultSet.getTimestamp("createdOn").toLocalDateTime();
                String nif = resultSet.getString("nif");
                int status = resultSet.getInt("status");

                return new Enterprise(id2, name, address, city, province, country, locale, createdOn, nif, status);
            }
        return null;

        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }


    }

    @Override
    public Enterprise getById(int id) throws EnterpriseNotFoundException{

        Enterprise enterprise = findById(id);

        if (enterprise != null){
            return enterprise;
        }

        throw new EnterpriseNotFoundException();

    }

    public boolean insert(Enterprise enterprise){

        String insertValues = "'"+enterprise.getId()+"','"+enterprise.getName()+"','"+enterprise.getAddress()+"','"+enterprise.getCity()+"','"+enterprise.getProvince()+"','"+enterprise.getCountry()+"','"+enterprise.getLocale()+"','"+String.valueOf(enterprise.getCreatedOn())+"','"+enterprise.getNif()+"','"+enterprise.getStatus()+"'";

        String SQLQuery ="INSERT INTO Enterprise (name,address,city,province,country,locale,createdOn,nif,status) VALUES ("+insertValues+")";

        Connection connection = this.connection.getConnection();
        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate(SQLQuery);
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return true;

    }

    public void updatePasswords(){

        Connection connection = this.connection.getConnection();
        try {

            Statement statement = connection.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = statement.executeQuery("SELECT * FROM User");
            while (rs.next()) {
                String firstname = rs.getString("firstName");
                String lastname = rs.getString("lastName");
                String concatenatedName = firstname+"_"+lastname;
                rs.updateString("password", generateSha1(concatenatedName));
                rs.updateRow();
            }

        }catch (SQLException e){
            e.printStackTrace();

        }

    }

    public boolean insertAlex(Enterprise enterprise){

        String insertValues = "'"+enterprise.getName()+"','"+enterprise.getAddress()+"','"+enterprise.getCity()+"','"+enterprise.getProvince()+"','"+enterprise.getCountry()+"','"+enterprise.getLocale()+"','"+String.valueOf(enterprise.getCreatedOn())+"','"+enterprise.getNif()+"','"+enterprise.getStatus()+"'";

        String sql ="INSERT INTO Enterprise (name,address,city,province,country,locale,createdOn,nif,status) VALUES ("+insertValues+")";

        Connection connection =this.connection.getConnection();

        try {

            Statement statement = connection.createStatement();
            statement.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()){
                 int idEnterprise = resultSet.getInt(1);
                enterprise.setId(idEnterprise);
                return true;
            }


        }catch (SQLException e){
            e.printStackTrace();

        }
        return false;
    }
    public boolean update(Enterprise enterprise){

        int id = enterprise.getId();
        String name = enterprise.getName();
        String address = enterprise.getAddress();
        String city = enterprise.getCity();
        String province = enterprise.getProvince();
        String country = enterprise.getCountry();
        String locale = enterprise.getLocale();
        String createdOn = String.valueOf(enterprise.getCreatedOn());
        String nif = enterprise.getNif();
        int status = enterprise.getStatus();

        Connection connection = this.connection.getConnection();

        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate("UPDATE Enterprise SET name='"+name+"',address='"+address+"',city='"+city+"',province='"+province+"',country='"+country+"',locale='"+locale+"',nif='"+nif+"'  WHERE id="+id);
            return true;
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        }

    }

    public void printUsers(){

        Connection connection = this.connection.getConnection();
        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT * FROM User");
            while (rs.next()) {
                int id = rs.getInt("id");
                String firstname = rs.getString("firstName");
                String lastname = rs.getString("lastName");
                String email = rs.getString("email");
                System.out.println("id: "+id);
                System.out.println("email: "+email);
                System.out.println("firstName: "+firstname);
                System.out.println("lastName: "+lastname+"\n");
            }

        }catch (SQLException e){
            e.printStackTrace();

        }
    }

    public String getPasswordFromId(int id){

        Connection connection = this.connection.getConnection();
        String SQLQuery = "SELECT * FROM User WHERE id="+id;

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQLQuery);
            if (rs.next()) {
                return rs.getString("password");
            }

        }catch (SQLException e){
            e.printStackTrace();

        }

        return null;
    }

    public String getPasswordFromEmail(String email){

        Connection connection = this.connection.getConnection();
        String SQLQuery = "SELECT * FROM User WHERE email='"+email+"'";

        try {

            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(SQLQuery);
            if (rs.next()) {
                return rs.getString("password");
            }

        }catch (SQLException e){
            e.printStackTrace();

        }

        return null;
    }

    public void insertUsersArray(User[] usersArray){

        Connection connection = this.connection.getConnection();


        for (int i=0;i<usersArray.length;i++){

            User user = usersArray[i];
            String sqlQuery = "INSERT INTO User(firstName,lastName,email,phoneNumber,password,active,createdOn,lastLogin,locale,idEnterprise,birthday) VALUES (?,?,?,?,?,?,?,?,?,1,?)";

            try {

                PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);

                preparedStatement.setString(1,user.getFirstName());
                preparedStatement.setString(2,user.getLastName());
                preparedStatement.setString(3,user.getEmail());
                preparedStatement.setString(4,user.getPhoneNumber());
                preparedStatement.setString(5,user.getPassword());
                preparedStatement.setBoolean(6,user.getActive());
                preparedStatement.setDate(7,java.sql.Date.valueOf(user.getCreatedOn().toLocalDate()));
                preparedStatement.setDate(8,java.sql.Date.valueOf(user.getLastLogin().toLocalDate()));
                preparedStatement.setString(9,user.getLocale());
                preparedStatement.setDate(10,java.sql.Date.valueOf(user.getBirthday()));

                preparedStatement.executeUpdate();



            }catch (SQLException e){
                e.printStackTrace();

            }


        }

    }

    public boolean login(){

        printUsers();
        System.out.println("Insert login firstName");
        Scanner scanner = new Scanner(System.in);
        String email = scanner.next();
        String passwordBD = getPasswordFromEmail(email);
        System.out.println("Introduce la contraseña del usuario con email esteve@test.com: "+email);
        String passwordIntroducida = scanner.next();

        if (generateSha1(passwordIntroducida).equalsIgnoreCase(passwordBD)){
            System.out.println("Login correcto");
            return true;
        }else {
            System.out.println("Login incorrecto");
            return false;
        }


    }

}