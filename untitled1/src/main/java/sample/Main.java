package sample;


import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {

        primaryStage.setTitle("Hello World");
        EnterpriseFormView enterpriseFormView = new EnterpriseFormView();
        enterpriseFormView.setAlignment(Pos.CENTER);
        Scene scene = new Scene(enterpriseFormView, 400, 400);
        scene.getStylesheets().add(Main.class.getResource("main.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
        Main.class.getResource("main.css").toExternalForm();

    }

}
