package sample;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Enterprise {

    private Integer id;
    private String name;
    private String address;
    private String city;
    private String province;
    private String country;
    private String locale;
    private LocalDateTime createdOn;
    private String nif;
    private int status;

    public Enterprise(String name, String address, String city, String province, String country, String locale, String nif) {

        this.id=null;
        this.name = name;
        this.address = address;
        this.city = city;
        this.province = province;
        this.country = country;
        this.locale = locale;
        this.nif = nif;
        this.createdOn = LocalDateTime.now();
        this.status = 1;

    }

    public Enterprise(Integer id,String name,String address,String city,String province, String country,String locale,LocalDateTime createdOn,String nif,int status){

        this.id=id;
        this.name=name;
        this.address=address;
        this.city=city;
        this.province=province;
        this.country=country;
        this.locale=locale;
        this.createdOn=createdOn;
        this.nif=nif;
        this.status=status;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

    public String getLocale() {
        return locale;
    }

    public String getCreatedOnISOString() {

        return createdOn.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public int getStatus() {
        return status;
    }

    public String getNif() {
        return nif;
    }

    public int isStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "nombre: "+name+"\n"+
                "address: "+address+"\n"+
                "city: "+city+"\n"+
                "province: "+province+"\n"+
                "country: "+country+"\n"+
                "locale: "+locale+"\n"+
                "nif: "+nif+"\n"+
                "created on: "+createdOn+"\n"+
                "status: "+status+"\n";
    }
}
